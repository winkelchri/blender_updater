# Blender Update Helper Script

Utility script for updating or installing Blender (https://blender.org) into the current directory.

It runs currently as a Python 3.7 (https://python.org) script and requires `click` (https://click.palletsprojects.com/en/7.x/).

The currently only supported platform is `Windows 64-Bit` but easily extenseable for other operating systems.


## CLI


### All available commands

You can see all available commands using `--help`.

```
$ update_blender.py --help

Usage: update_blender.py [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  cleanup  Removes all files from the blender dir
  update   Update the current blender version to the...
```


### Cleanup command

The `cleanup` command is being used during an update but can be triggered automatically.

```
update_blender.py cleanup --help

Usage: update_blender.py cleanup [OPTIONS]

  Removes all files from the blender dir

Options:
  --fake     Just show the files who should be deleted.
  --debug    Show debug messages
  --help     Show this message and exit.
```

### Update command


The `update` command will install or update blender within the current directory. If not specified, the Blender version `2.80` will be used.

```
update_blender.py update --help

Usage: update_blender.py update [OPTIONS]

  Update the current blender version to the recent one.

Options:
  --force / --no-force  Skip version check and forces an update
  --version TEXT        Specify the desired blender version. Default is 2.80
  --debug               Show debug messages
  --help                Show this message and exit.

```
