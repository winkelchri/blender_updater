import urllib
import urllib.request
import re
import pathlib
import shutil
import glob
import zipfile
import platform
import logging

import click

LOGGING_FORMAT = "%(asctime)s %(name)s[%(levelname)s|%(lineno)s]: %(message)s "
logging.basicConfig(format=LOGGING_FORMAT)

BLENDER_DOWNLOAD_URL = "https://builder.blender.org/download"
VERSION_FILE = pathlib.Path("current_version.txt")
EXCLUDE_FROM_CLEANUP = [".git", ".gitignore", "README.md"]


class BlenderUpdater():
    def __init__(self, download_url=BLENDER_DOWNLOAD_URL, version_file=VERSION_FILE, loglevel='INFO'):
        self.base_url = download_url
        self.version_file = version_file
        self.current_dir =  pathlib.Path(__file__).absolute().parent

        self.__installed_version = None

        self.log = logging.getLogger('update_blender')
        self.log.setLevel(loglevel)

    def get_latest_blender_download_link(self, blender_version):
        ''' Gets the latest Blender zip file to download.
            Returns a tuple with the download url and the version of the file.
        '''

        bits, linkage = platform.architecture()

        if bits == '64bit' and "windows" in linkage.lower():
            platform_suffix = "win64"
        else:
            raise NotImplementedError(f"Unsupported platform: {linkage} {bits}")

        response = urllib.request.urlopen(self.base_url)
        html_data = response.read()
        match = re.search(
            f'blender-{blender_version}-([\w]+)-{platform_suffix}\.zip'.encode('utf-8'),
            html_data
        )

        if match is not None:
            zip_file = match.group(0)
            version = match.group(1)

            download_url = f"{self.base_url}/{zip_file.decode('utf-8')}"
            self.log.debug(f"Got latest Blender download URL: {download_url}")

            return (download_url, version)

        else:
            self.log.error(f"No download link found!")
            return None

    @property
    def installed_version(self):
        ''' Getter for the installed version of Blender. '''

        if self.version_file.is_file():
            with self.version_file.open("rb") as version_file:
                installed_version = version_file.read()
                self.__installed_version = installed_version

        return self.__installed_version

    @installed_version.setter
    def installed_version(self, version):
        ''' Setter for the installed version of Blender. '''

        with self.version_file.open("wb") as version_file:
            version_file.write(version)

    def update(self, force=False, version="2.80"):
        ''' Start the update process.

            Includes cleanup, download and installation of the package.
        '''

        download_url, download_version = self.get_latest_blender_download_link(blender_version=version)
        installed_version = self.installed_version

        if force:
            self.log.info("Forcing download")

        # Download the blender file
        if force or (download_version != installed_version):
            self.log.info(f"Downloading the latest Blender {version} version: {download_version.decode('utf-8')} ...")

            # Downloads the latest blender file and saves into a temporary file
            download_file, _ = urllib.request.urlretrieve(download_url)
            self.log.debug(f"... done. {download_file}")

            # Cleanup the current directory
            self.cleanup()

            # Unpack the new blender file into current directory
            self.install(download_file)

            # Remove temporary download file
            urllib.request.urlcleanup()

            # Store the new downloaded version
            self.installed_version = download_version

        # Already on current version
        else:
            self.log.info((
                f"Current Blender {version} Build {installed_version.decode('utf-8')} "
                "is the most recent one. -> Skip Update"
            ))

    def install(self, zip_file):
        ''' Unpacks the downloaded zip file
            and moves the content into the current directory.
        '''

        def move_on_top(subfolder):
            ''' Moves the content of the given subfolder into the current directory. '''

            self.log.debug(f"Move everything from {subfolder} to here.")
            for file in subfolder.iterdir():
                src = file
                dst = pathlib.Path(*list(file.parts[1:]))
                self.log.debug(f"Moving {src} -> {dst}")
                shutil.move(src, dst)

            self.log.debug(f"Remove {subfolder}")
            subfolder.rmdir()

        def extract(skip_extract=False):
            ''' Extracts the content of the downloaded zip file.

                Returns the containing subfolder for later file moving.
            '''

            # The zip file of the blender directory usually contains a single directory
            # (with a git name) containing all the files. This will be the name of this
            # directory.
            extracted_subfolder = None

            self.log.info(f"Unpacking downloaded file {zip_file} into {self.current_dir}")
            with zipfile.ZipFile(zip_file) as zfile:

                if extracted_subfolder is None:
                    extracted_subfolder = pathlib.Path(zfile.namelist()[0].split('/')[0])

                # Unpack every file one by one ...
                for file in zfile.namelist():
                    src = file
                    dst = "."
                    if not skip_extract:
                        self.log.debug(f"Extract: {src}")
                        zfile.extract(src, dst)
                    else:
                        self.log.debug(f"Skip Extract: {src}")
            return extracted_subfolder

        subfolder = extract(skip_extract=False)
        move_on_top(subfolder)

    def cleanup(self, fake=False, before_update=True):
        ''' Removes all files from the current directory.

            If `before_update` is True, the current_version file
            will not be deleted.

            Skips own and EXCLUDE_FROM_CLEANUP files.
        '''

        # Basic - own - exclusions
        exclude_files = [
            pathlib.Path(__file__),
        ]

        if before_update is True:
            exclude_files.append(self.version_file.absolute())

        # Add additional exclusions
        for file_or_path in EXCLUDE_FROM_CLEANUP:
            fop = pathlib.Path(file_or_path)
            exclude_files.append(fop.absolute())

        self.log.warning(f"Cleaning up install directory: {self.current_dir}")

        for file_or_path in self.current_dir.iterdir():
            if file_or_path in exclude_files:
                continue

            if fake:
                self.log.debug(f"Not-Deleting '{file_or_path}''")
            else:
                self.log.debug(f"Deleting '{file_or_path}''")
                if file_or_path.is_file():
                    file_or_path.unlink()

                if file_or_path.is_dir():
                    shutil.rmtree(file_or_path, ignore_errors=True)


@click.group()
def cli():
    pass


@cli.command(help="Update the current blender version to the recent one.")
@click.option('--force/--no-force', default=False, help="Skip version check and forces an update")
@click.option('--version', default="2.80", help="Specify the desired blender version. Default is 2.80")
@click.option('--debug', default=False, is_flag=True, help="Show debug messages")
def update(force, version, debug):
    if debug:
        bup = BlenderUpdater(loglevel='DEBUG')
    else:
        bup = BlenderUpdater()
    bup.update(force=force, version=version)


@cli.command(help="Removes all files from the blender dir")
@click.option('--fake', is_flag=True, default=False, help="Just show the files who should be deleted.")
@click.option('--debug', default=False, is_flag=True, help="Show debug messages")
def cleanup(fake, debug):
    if debug:
        bup = BlenderUpdater(loglevel='DEBUG')
    else:
        bup = BlenderUpdater()
    bup.cleanup(fake=fake, before_update=False)


if __name__ == '__main__':
    cli()
